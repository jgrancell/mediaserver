#!/usr/bin/env sh

if [ $(curl -s https://ipleak.net/json/ | grep "country_code" | cut -d\" -f4) = "CA" ]; then
  exit 0
else
  exit 1
fi
