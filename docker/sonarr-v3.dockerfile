FROM alpine:3.11

ARG VERSION=3.0.3.688
COPY docker/src/sonarr-v3/sonarr_run.sh /usr/local/bin/sonarr_run.sh

## Python setup and pre-requisite build
RUN chmod +x /usr/local/bin/sonarr_run.sh \
  && apk add --no-cache mono --repository http://dl-3.alpinelinux.org/alpine/edge/testing \
  && apk add --no-cache libmediainfo sqlite curl \
  && adduser -s /bin/ash -D -u 1000 sonarr \
  && mkdir -p /opt/sonarr \
  && chown -R sonarr:sonarr /opt/sonarr \
  && mkdir -p /data \
  && chown -R sonarr:sonarr /data

## Running all further commands as the sabnzbd user
USER sonarr
WORKDIR /opt/sonarr

RUN wget -O sonarr.tgz http://download.sonarr.tv/v3/phantom-develop/$VERSION/Sonarr.phantom-develop.$VERSION.linux.tar.gz \
  && tar -xzvf sonarr.tgz \
  && rm -f sonarr.tgz \
  && cp -a Sonarr/* ./ \
  && rm -rf Sonarr \
  && mkdir -p /home/sonarr/.config/Sonarr

EXPOSE 8989/tcp

ENTRYPOINT ["/bin/sh", "/usr/local/bin/sonarr_run.sh"]
