FROM alpine:3.11

ARG VERSION
COPY docker/src/radarr/radarr_run.sh /usr/local/bin/radarr_run.sh

## Python setup and pre-requisite build
RUN chmod +x /usr/local/bin/radarr_run.sh \
  && apk add --no-cache mono --repository http://dl-3.alpinelinux.org/alpine/edge/testing \
  && apk add --no-cache libmediainfo sqlite curl \
  && adduser -s /bin/ash -D -u 1000 radarr \
  && mkdir -p /opt/radarr \
  && chown -R radarr:radarr /opt/radarr \
  && mkdir -p /data \
  && chown -R radarr:radarr /data

## Running all further commands as the sabnzbd user
USER radarr
WORKDIR /opt/radarr

RUN wget -O radarr.tgz https://github.com/Radarr/Radarr/releases/download/v$VERSION/Radarr.develop.$VERSION.linux.tar.gz \
  && tar -xzvf radarr.tgz \
  && rm -f radarr.tgz \
  && cp -a Radarr/* ./ \
  && rm -rf Radarr \
  && mkdir -p /home/radarr/.config/Radarr

EXPOSE 8989/tcp

ENTRYPOINT ["/bin/sh", "/usr/local/bin/radarr_run.sh"]
