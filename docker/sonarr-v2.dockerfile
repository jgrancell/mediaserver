FROM alpine:3.11

COPY docker/src/sonarr-v2/sonarr_run.sh /usr/local/bin/sonarr_run.sh

## Python setup and pre-requisite build
RUN chmod +x /usr/local/bin/sonarr_run.sh \
  && apk add --no-cache mono --repository http://dl-3.alpinelinux.org/alpine/edge/testing \
  && apk add --no-cache libmediainfo sqlite curl \
  && adduser -s /bin/ash -D -u 1000 sonarr \
  && mkdir -p /opt/sonarr \
  && chown -R sonarr:sonarr /opt/sonarr \
  && mkdir -p /data \
  && chown -R sonarr:sonarr /data

## Running all further commands as the sabnzbd user
USER sonarr
WORKDIR /opt/sonarr

RUN wget -O sonarr.tgz http://update.sonarr.tv/v2/master/mono/NzbDrone.master.tar.gz \
  && tar -xzvf sonarr.tgz \
  && rm -f sonarr.tgz \
  && cp -a NzbDrone/* ./ \
  && rm -rf NzbDrone \
  && mkdir -p /home/sonarr/.config/NzbDrone

EXPOSE 8989/tcp

ENTRYPOINT ["/bin/sh", "/usr/local/bin/sonarr_run.sh"]
