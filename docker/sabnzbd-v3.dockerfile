FROM alpine:3.12 as par2build

## Pulling and building par2cmdline from source
RUN apk add --no-cache git automake autoconf build-base \
  && git clone https://github.com/Parchive/par2cmdline.git /build \
  && cd /build \
  && aclocal \
  && automake --add-missing \
  && autoconf \
  && ./configure \
  && make \
  && make install

FROM alpine:3.12

ARG SABNZBD_VERSION=3.3.1

COPY --from=par2build /usr/local/bin/par2 /usr/local/bin/par2
COPY docker/src/sabnzbd/sabnzbd3_run.sh /usr/local/bin/sabnzbd_run.sh

## Python setup and pre-requisite build
RUN chmod +x /usr/local/bin/sabnzbd_run.sh \
  && apk add --no-cache python3 curl \
  && apk add --no-cache --virtual .builddeps python3-dev build-base gcc wget py3-pip openssl-dev libffi-dev \
  && apk add --no-cache --virtual .sabdeps unzip p7zip unrar \
  && ln -sf /usr/local/bin/par2 /usr/local/bin/par2create \
  && ln -sf /usr/local/bin/par2 /usr/local/bin/par2verify \
  && ln -sf /usr/local/bin/par2 /usr/local/bin/par2repair \
  && adduser -s /bin/ash -D -u 1000 sabnzbd \
  && mkdir -p /opt/sabnzbd \
  && chown -R sabnzbd:sabnzbd /opt/sabnzbd \
  && mkdir -p /data/usenet \
  && chown -R sabnzbd:sabnzbd /data

WORKDIR /opt/sabnzbd

RUN wget -O sabnzbd.tgz https://github.com/sabnzbd/sabnzbd/releases/download/$SABNZBD_VERSION/SABnzbd-$SABNZBD_VERSION-src.tar.gz \
  && tar -xzvf sabnzbd.tgz \
  && rm -f sabnzbd.tgz \
  && cp -a SABnzbd-$SABNZBD_VERSION/* ./ \
  && rm -rf SABnzbd-$SABNZBD_VERSION

RUN pip install -r requirements.txt \
 && chown -R sabnzbd:sabnzbd /opt/sabnzbd

USER sabnzbd

EXPOSE 8080/tcp
EXPOSE 9090/tcp

ENTRYPOINT ["/bin/sh", "/usr/local/bin/sabnzbd_run.sh"]
